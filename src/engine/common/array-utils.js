export default {
    fromRange(count, mapFn) {
        return Array.from(Array(count), (_, i) => mapFn(i));
    },

    compare(obj1, obj2, comparators) {
        for (let comparator of comparators) {
            const f1 = comparator(obj1);
            const f2 = comparator(obj2);
            
            if (f1 < f2)
                return -1;

            if (f1 > f2)
                return 1;
        }

        return 0;
    }
}