export default class BattleTeam {
    constructor(id) {
        this.id = id;
        this.units = [];
        this.statistics = { kills: 0, deaths: 0 };
    }

    toString() {
        return `t${this.id}`;
    }
}