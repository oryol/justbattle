import Point from "./math/point";
import SimpleRect from "./math/simple-rect";
import RandomUtils from "./math/random-utils";
import BattleFormat from "./battle-format";
import EngineConstants from "./engine-constants";

export default class BattleMap {
    constructor(name, format, width, height, spawnPoints) {
        this.name = name;
        this.format = format;

        this.width = width;
        this.height = height;
        this.rect = new SimpleRect(0, this.width, 0, this.height);

        this.spawnPoints = spawnPoints;        

        BattleMap.mapsByName[name] = this;
    }

    toString() {
        return this.name;
    }

    static getMapForFormat(format) {
        return RandomUtils.getItem(BattleMap.mapsByFormat[format]);
    }

    static getMapByName(name) {
        return BattleMap.mapsByName[name];
    }

    getSpawnPoint(unit) {
        const teamIdx = unit.team.id - 1;
        const unitIdx = unit.indexInTeam;
        const pointIdx = teamIdx * this.format.playerInTeam + unitIdx;
        console.log("Spawn point idx: " + pointIdx);
        return this.spawnPoints[pointIdx];
    }
}

BattleMap.mapsByName = {};

BattleMap.mapsByFormat = {
    [BattleFormat.formats._1_1]: [
        new BattleMap("m_1_1__1", BattleFormat.formats._1_1,
            EngineConstants.mapNormalSize, EngineConstants.mapNormalSize, [
                new Point(EngineConstants.unitSize + 10, EngineConstants.mapNormalSize / 2),
                new Point(EngineConstants.mapNormalSize - EngineConstants.unitSize - 10, EngineConstants.mapNormalSize / 2)
            ]
        ),
        new BattleMap("m_1_1__2", BattleFormat.formats._1_1,
            EngineConstants.mapNormalSize * 1.5, EngineConstants.mapNormalSize, [
                new Point(EngineConstants.unitSize + 10, EngineConstants.unitSize + 10),
                new Point(EngineConstants.mapNormalSize * 1.5 - EngineConstants.unitSize - 10, EngineConstants.mapNormalSize - EngineConstants.unitSize - 10)
            ]
        )
    ],
    [BattleFormat.formats._4p]: [
        new BattleMap("m_4p__1", BattleFormat.formats._4p,
            EngineConstants.mapNormalSize, EngineConstants.mapNormalSize, [
                new Point(EngineConstants.unitSize + 10, EngineConstants.mapNormalSize / 2),
                new Point(EngineConstants.mapNormalSize / 2, EngineConstants.unitSize + 10),
                new Point(EngineConstants.mapNormalSize - EngineConstants.unitSize - 10, EngineConstants.mapNormalSize / 2),
                new Point(EngineConstants.mapNormalSize / 2, EngineConstants.mapNormalSize - EngineConstants.unitSize - 10)
            ]
        ),
        new BattleMap("m_4p__2", BattleFormat.formats._4p,
            EngineConstants.mapNormalSize * 0.8, EngineConstants.mapNormalSize * 1.5, [
                new Point(EngineConstants.unitSize + 10, EngineConstants.mapNormalSize * 0.25),
                new Point(EngineConstants.unitSize + 10, EngineConstants.mapNormalSize * 0.75),
                new Point(EngineConstants.mapNormalSize * 0.8 - EngineConstants.unitSize - 10, EngineConstants.mapNormalSize * 0.25),
                new Point(EngineConstants.mapNormalSize * 0.8 - EngineConstants.unitSize - 10, EngineConstants.mapNormalSize * 0.75),
            ]
        )
    ]
};
