import EngineConstants from "./engine-constants";

import Point from "./math/point";
import Vector from "./math/vector";
import SimpleRect from "./math/simple-rect";

import SkillFactory from "./skills/skill-factory";
import SkillStage from "./skills/skill-stage";
import SkillEffect from "./skills/skill-effect";

export default class BattleUnit {
    constructor() {
        this.moveTarget = null;
        this.moveDirection = null;
        this.moveVector = null;
        
        this.currentSkill = null;
        this.skillTarget = null;
        this.lastDamageSource = null;

        this.timeToRespawn = 0;
        this.isInBattle = false;

        this.effects = {
            attackBuff: new SkillEffect("attackBuff", 0, 0),
            defenceBuff: new SkillEffect("defenceBuff", 0, 0),
            healPowerBuff: new SkillEffect("healPowerBuff", 0, 0)
        };

        this.statistics = {kills: 0, deaths: 0};
    }


    static createFromDbUnit(u) {
        const unit = new BattleUnit();
        unit.id = u.host_unitId;
        
        unit.maxHealthPoints = polynom(u.stats.maxHealthPoints, 1.25, -11, 80, 30);
        unit.attack = polynom(u.stats.attack, 0.2, -2.5, 25, 2);
        unit.defence = polynom(u.stats.defence, 0.21, -2.5, 20, -15);
        unit.healPower = polynom(u.stats.healPower, 0.2, -3, 15, 0);
        unit.healthPoints = unit.maxHealthPoints;

        unit.skills = u.skills.map(s => SkillFactory
            .createSkill(s.name, unit, s.level, {key: s.key}));

        return unit;
    }


    static createFromInitialState(u, battle) {
        const unit = new BattleUnit();
        unit.id = u.id;        

        unit.position = Point.fromArray(u.p);
        unit.updateRect();

        unit.team = battle.teams[u.t[0] - 1];        
        unit.team.units.push(unit);
        unit.indexInTeam = u.t[1];

        unit.maxHealthPoints = u.st[0];
        unit.attack = u.st[1];
        unit.defence = u.st[2];
        unit.healPower = u.st[3];

        unit.skills = u.s.map(s => SkillFactory.createSkillFromInitialState(s, unit));

        return unit;
    }

    getInitialState() {
        return {
            id: this.id,
            p: this.position.toArray(),
            t: [this.team.id, this.indexInTeam],
            st: [this.maxHealthPoints, this.attack, this.defence, this.healPower],
            s: this.skills.map(s => SkillFactory.getInitialSkillState(s))
        }
    }


    updateFromState(u) {
        this.position = Point.fromArray(u.p);
        this.updateRect();

        this.moveVector = Vector.fromArray(u.m);        
        this.healthPoints = u.h;
        
        this.skillTarget = u.t ? this.battle.unitsById[u.t] : null;
        this.currentSkill = u.cs >= 0 ? this.skills[u.cs] : null;

        for (let [skillIdx, skill] of this.skills.entries())
            skill.updateFromState(u.s[skillIdx]);

        this.timeToRespawn = u.lf;
        this.isInBattle = u.lf === -1;
        if (this.isInBattle)
            this.timeToRespawn = 0;

        this.effects.attackBuff = u.e[0];
        this.effects.defenceBuff = u.e[1];
        this.effects.healPowerBuff = u.e[2];
    }

    getState() {
        return {
            id: this.id,
            p: this.position.toArray(),
            m: Vector.toArray(this.moveVector),
            h: this.healthPoints,
            t: this.skillTarget ? this.skillTarget.id : null,
            s: this.skills.map(s => s.getState()),
            cs: this.currentSkill ? this.skills.indexOf(this.currentSkill) : -1,
            lf: this.isInBattle ? -1 : this.timeToRespawn,
            e: [
                this.effects.attackBuff, this.effects.defenceBuff, this.effects.healPowerBuff
            ]
        };
    }


    get effectiveAttack() {
        return this.attack * this.effects.attackBuff.buffMultiplier;
    }

    get effectiveDefence() {
        return this.defence * this.effects.defenceBuff.buffMultiplier;
    }

    get effectiveHealPower() {
        return this.healPower * this.effects.healPowerBuff.buffMultiplier;
    }


    prepareMove(elapsedTime) {
        this.newPosition = this.position;
        this.newRect = this.rect;

        if (!this.moveVector)
            return;

        const newPosition = this.position.add(this.moveVector.multiple(elapsedTime * EngineConstants.unitSpeed));
        const newRect = SimpleRect.createForCircle(newPosition, EngineConstants.unitSize / 2);

        if (!this.battle.map.rect.isInclude(newRect))
            return;

        this.newPosition = newPosition;
        this.newRect = newRect;
    }

    isCollideAfterMove(otherUnit) {
        if (!this.newRect.isCollide(otherUnit.newRect))
            return false;

        const d = this.newPosition.getVectorTo(otherUnit.newPosition).length;
        return d < EngineConstants.unitSize;
    }

    undoMove() {
        this.newPosition = this.position;
        this.newRect = this.rect;
    }

    move() {
        if (this.position === this.newPosition)
            return false;

        this.position = this.newPosition;
        this.rect = this.newRect;

        this.interruptSkillCast();

        return true;
    }


    setMoveDirection(d) {
        if (!d) {
            this.moveDirection = null;
        } else {
            this.moveDirection = d;
            this.moveTarget = null;            
        }

        this.updateMoveVector();
    }

    setMoveTarget(t) {
        if (!t) {
            this.moveTarget = null;
        } else {
            this.moveTarget = t;
            this.moveDirection = null;
        }

        this.updateMoveVector();
    }

    updateMoveVector() {
        if (this.moveTarget) {
            this.moveVector = this.position.getVectorTo(this.moveTarget).normalize();
        } else if (this.moveDirection) {
            this.moveVector = this.moveDirection.normalize();
        } else {
            this.moveVector = null;
        }
    }

    updateRect() {
        this.rect = SimpleRect.createForCircle(this.position, EngineConstants.unitSize / 2);
    }


    interruptSkillCast() {
        if (!this.currentSkill)
            return;

        if (this.currentSkill.stage !== SkillStage.Cast)
            return;

        this.currentSkill.reset();
        this.currentSkill = null;
    }
}

function polynom(x, r3, r2, r1, r0) {
    const x2 = x * x;
    const x3 = x * x2;
    return (r3 || 0) * x3 + (r2 || 0) * x2 + (r1 || 0) * x + (r0 || 0);
}
