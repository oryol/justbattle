export default {
    unitSize: 70,
    unitSpeed: 0.25,

    mapNormalSize: 1000,

    globalCooldownTime: 300,
    respawnTime: 5000,

    cooldownTime1: 6000,
    cooldownTime2: 18000,
    cooldownTime3: 30000,
    cooldownTime4: 60000,

    skillDistance1: 350,
    skillDistance2: 400,
    skillDistance3: 430,

    castTime1: 500,
    castTime2: 900,
    castTime3: 1900,

    buffTime: 10000,
    controlTime: 14000,

    BattleStatuses: {
        connect: 0,
        prestart: 1,
        active: 2,
        finished: 3
    }
};
