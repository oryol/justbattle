export default class Vector {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    static fromObject(obj) {
        return obj ? new Vector(obj.x || 0, obj.y || 0) : null;
    }

    static fromArray(arr) {
        return arr ? new Vector(arr[0] || 0, arr[1] || 0) : null;
    }

    static toArray(v) {
        return v ? v.toArray() : null;
    }

    toArray() {
        return [this.x, this.y];
    }

    toString() {
        return `(${this.x}; ${this.y})`;
    }

    get length() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    get sqrLength() {
        return this.x * this.x + this.y * this.y;
    }

    normalize() {
        const length = this.length;
        return new Vector(this.x / length, this.y / length);
    }

    multiple(r) {
        return new Vector(this.x * r, this.y * r);
    }
}