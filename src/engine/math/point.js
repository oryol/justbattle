import Vector from "./vector";

export default class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    static fromObject(obj) {
        return obj ? new Point(obj.x || 0, obj.y || 0) : null;
    }

    static fromArray(arr) {
        return arr ? new Point(arr[0] || 0, arr[1] || 0) : null;
    }

    static toArray(p) {
        return p ? p.toArray() : null;
    }

    toArray() {
        return [this.x, this.y];
    }

    toString() {
        return `(${this.x}; ${this.y})`;
    }

    getVectorTo(p) {
        return new Vector(p.x - this.x, p.y - this.y);
    }

    add(v) {
        return new Point(this.x + v.x, this.y + v.y);
    }
}