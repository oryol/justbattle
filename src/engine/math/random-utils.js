export default {
    intFromRange(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    },

    getItem(arr) {
        const idx = Math.floor(Math.random() * arr.length);
        return arr[idx];
    }
};
