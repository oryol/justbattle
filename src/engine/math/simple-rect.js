export default class SimpleRect {
    constructor(x1, x2, y1, y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
    }

    static createForCircle(p, r) {
        return new SimpleRect(p.x - r, p.x + r, p.y - r, p.y + r);
    }

    toString() {
        return `[(${this.x1} - ${this.x2}) :: (${this.y1} - ${this.y2})]`;
    }

    get width() {
        return this.x2 - this.x1;
    }

    get height() {
        return this.y2 - this.y1;
    }

    isCollide(other) {
        if (other.x1 > this.x2 || this.x1 > other.x2)
            return false;

        if (other.y1 > this.y2 || this.y1 > other.y2)
            return false;

        return true;
    }

    isInclude(other) {
        if (other.x1 < this.x1 || other.x2 > this.x2)
            return false;

        if (other.y1 < this.y1 || other.y2 > this.y2)
            return false;

        return true;
    }

    isIncludePoint(p) {
        if (p.x < this.x1 || p.x > this.x2)
            return false;

        if (p.y < this.y1 || p.y > this.y2)
            return false;

        return true;
    }

    getDistanceTo(p) {
        const dx = getDistanceToBorder(this.x1, this.x2, p.x);
        const dy = getDistanceToBorder(this.y1, this.y2, p.y);

        if (dx === 0)
            return dy;

        if (dy === 0)
            return dx;

        return Math.sqrt(dx * dx + dy * dy);
    }
}

function getDistanceToBorder(b1, b2, p) {
    if (p < b1)
        return b1 - p;

    if (p > b2)
        return p - b2;

    return 0;
}