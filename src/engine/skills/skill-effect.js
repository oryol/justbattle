export default class SkillEffect {
    constructor(name, level, remainedTime) {
        this.name = name;
        this.level = level;
        this.remainedTime = remainedTime;
    }

    get isActive() {
        return this.remainedTime > 0;
    }

    get buffMultiplier() {
        if (this.remainedTime <= 0)
            return 1;

        return 1.5 + 0.1 * this.level;
    }

    update(elapsedTime) {
        if (this.remainedTime <= 0) 
            return;

        this.remainedTime -= elapsedTime;
        if (this.remainedTime <= 0) {
            this.remainedTime = 0;
            this.level = 0;
        }
    }

    applyToUnit(unit) {
        const unitEffect = unit.effects[this.name];
        if (unitEffect.level > this.level)
            return;

        if (unitEffect.level < this.level) {
            unitEffect.level = this.level;
            unitEffect.remainedTime = this.remainedTime;            
        } else if (this.remainedTime > unitEffect.remainedTime) {
            unitEffect.remainedTime = this.remainedTime;
        }
    }
}
