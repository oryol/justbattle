import EngineConstants from "../engine-constants";

import UnitSkill from "./unit-skill";
import SkillEffect from "./skill-effect";
import SkillTarget from "./skill-target";


class SupportSkill extends UnitSkill {
    constructor(effectName, name, unit, level) {
        super(name, unit, level);

        this.effect = new SkillEffect(effectName, level, EngineConstants.buffTime);
        this.skillTarget = SkillTarget.Self;
        this.cooldownTime = EngineConstants.cooldownTime4;
    }

    execute() {
        this.effect.applyToUnit(this.unit);
    }
}

class AttackBuffSkill extends SupportSkill {
    constructor(unit, level) {
        super("attackBuff", "s1", unit, level);
    }
}

class DefenceBuffSkill extends SupportSkill {
    constructor(unit, level) {
        super("defenceBuff", "s2", unit, level);
    }
}

class HealPowerBuffSkill extends SupportSkill {
    constructor(unit, level) {
        super("healPowerBuff", "s3", unit, level);
    }
}


export default [
    { name: "s1", skill: AttackBuffSkill },
    { name: "s2", skill: DefenceBuffSkill },
    { name: "s3", skill: HealPowerBuffSkill }
];
