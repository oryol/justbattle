import EngineConstants from "../engine-constants";

import UnitSkill from "./unit-skill";
import SkillTarget from "./skill-target";

class HealSkill extends UnitSkill {
    constructor(name, unit, level) {
        super(name, unit, level);

        this.skillTarget = SkillTarget.Self;
        this.multiplier = 1;
    }

    execute() {
        for (let target of this.targets)
            this.heal(target);
    }

    heal(target) {
        const hlp = this.unit.effectiveHealPower;
        const def = target.effectiveDefence;
        const heal = this.multiplier * hlp * (hlp / (hlp + def));

        if (target.healthPoints < target.maxHealthPoints) {
            target.healthPoints += heal;
            if (target.healthPoints > target.maxHealthPoints)
                target.healthPoints = target.maxHealthPoints;
        }

        this.showInfoMessage(`You recovered ${heal} HP`, target);
    }
}

class SimpleHealSkill extends HealSkill {
    constructor(unit, level) {
        super("h1", unit, level);

        this.multiplier = 0.2 + 0.05 * this.level;
        this.cooldownTime = EngineConstants.cooldownTime1;
        this.castTime = EngineConstants.castTime1;
    }
}

class HighHealSkill extends HealSkill {
    constructor(unit, level) {
        super("h2", unit, level);

        this.multiplier = 1 + 0.2 * this.level;
        this.castTime = EngineConstants.castTime3; 
        this.cooldownTime = EngineConstants.cooldownTime3;
    }
}

class AllyHealSkill extends HealSkill {
    constructor(unit, level) {
        super("h3", unit, level);

        this.skillTarget = SkillTarget.Ally;
        this.multiplier = 0.85 + 0.15 * this.level;
        this.setDistance(EngineConstants.skillDistance3);
        this.castTime = EngineConstants.castTime2;
        this.cooldownTime = EngineConstants.cooldownTime2;
    }
}

export default [
    { name: "h1", skill: SimpleHealSkill },
    { name: "h2", skill: HighHealSkill },
    { name: "h3", skill: AllyHealSkill }
];
