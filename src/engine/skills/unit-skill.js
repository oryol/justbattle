import SimpleRect from "../math/simple-rect";

import EngineConstants from "../engine-constants";

import SkillStage from "./skill-stage";
import SkillTarget from "./skill-target";


const errors = {
    noTarget: "Target is not selected",
    wrongTarget: "Wrong target",
    targetIsTooFar: "Target is too far"
};


export default class UnitSkill {
    constructor(name, unit, level) {
        // this doesn't work with UglifyJS
        /*if (new.target === UnitSkill)
            throw new TypeError("UnitSkill can not be instantiated directly");*/
        
        this.unit = unit;
        this.name = name;
        this.level = level;

        this.castTime = null;
        this.prepareTime = 300;
        this.cooldownTime = 0;
        
        this.stageTime = null;
        this.stageRemainedTime = null;
        this.stage = SkillStage.Ready;

        this.isAoe = false;
        this.skillTarget = SkillTarget.Self;
        this.distance = 0;
        this.sqrDistance = 0;

        this.singleTarget = null;
        this.targets = null;
    }


    updateFromState(s) {
        this.stage = s.s;
        this.stageTime = s.tm[0];
        this.stageRemainedTime = s.tm[1];
        this.singleTarget = s.tg ? this.unit.battle.unitsById[s.tg] : null;
    }

    getState() {
        return {
            s: this.stage,
            tm: [this.stageTime, this.stageRemainedTime],
            tg: this.singleTarget ? this.singleTarget.id : null
        };
    }


    setDistance(d) {
        this.distance = d;
        this.sqrDistance = d * d;
    }

    use() {
        if (this.stage !== SkillStage.Ready)
            return;

        if (!this.saveSingleTarget())
            return;

        this.unit.currentSkill = this;
        if (this.castTime === 0) {
            this.saveTargets();
            this.stage = SkillStage.Prepare;
            this.stageTime = this.prepareTime;
        } else {
            this.stage = SkillStage.Cast;
            this.stageTime = this.castTime;
        }

        this.stageRemainedTime = this.stageTime;
    }

    update(elapsedTime) {
        if (this.stage === SkillStage.Ready)
            return;

        this.stageRemainedTime -= elapsedTime;
        if (this.stageRemainedTime > 0)
            return;

        switch (this.stage) {
            case SkillStage.Cast:
                this.stageTime = this.prepareTime;
                this.stage = SkillStage.Prepare;
                if (this.unit.battle.serverMode)
                    this.saveTargets();
                break;

            case SkillStage.Prepare:
                this.stageTime = EngineConstants.globalCooldownTime;
                this.stage = SkillStage.Dispose;
                if (this.unit.battle.serverMode)
                    this.execute();
                break;

            case SkillStage.Dispose:
                this.stage = SkillStage.Cooldown;
                this.stageTime = this.cooldownTime;
                this.unit.currentSkill = null;
                break;

            case SkillStage.Cooldown:
                this.stage = SkillStage.Ready;
                this.stageTime = null;
                break;
        }

        this.stageRemainedTime = this.stageTime;
    }

    reset() {
        this.stage = SkillStage.Ready;
        this.stageTime = null;
        this.singleTarget = null;
        this.targets = null;
    }

    saveSingleTarget() {
        if (this.isAoe)
            return true;

        if (this.skillTarget === SkillTarget.Self) {
            this.singleTarget = this.unit;
            return true;
        }

        if (this.skillTarget === SkillTarget.Enemy) {
            const target = this.unit.skillTarget;
            if (!target) {
                this.showInfoMessage(errors.noTarget);
                return false;
            }

            if (target.team.id === this.unit.team.id) {
                this.showInfoMessage(errors.wrongTarget);
                return false;                
            }

            if (this.unit.position.getVectorTo(target.position).sqrLength > this.sqrDistance) {
                this.showInfoMessage(errors.targetIsTooFar);
                return false;
            }

            this.singleTarget = target;
            return true;
        }

        if (this.skillTarget === SkillTarget.Ally) {
            const target = this.unit.skillTarget;
            if (!target) {
                this.singleTarget = this.unit;
                return true;
            }

            if (target.team.id !== this.unit.team.id) {
                this.singleTarget = this.unit;
                return true;
            }

            if (this.unit.position.getVectorTo(target.position).sqrLength > this.sqrDistance) {
                this.showInfoMessage(errors.targetIsTooFar);
                return false;
            }

            this.singleTarget = target;
            return true;
        }

        return false;
    }

    saveTargets() {
        if (this.isAoe) {
            const aoeRect = SimpleRect.createForCircle(this.unit.position, this.distance);
            this.targets = this.unit.battle.units.filter(u => this.isValidAoeTarget(u, aoeRect));
        } else {
            if (this.unit.position.getVectorTo(this.singleTarget.position).sqrLength <= this.sqrDistance) {
                this.targets = [this.singleTarget];
            } else {
                this.showError(errors.targetIsTooFar);
                this.reset();
                this.unit.currentSkill = null;
            }
        }
    }

    isValidAoeTarget(u, aoeRect) {
        const myTeam = this.unit.team.id;
        const otherTeam = u.team.id;

        if (this.skillTarget === SkillTarget.Enemy) {
            if (myTeam === otherTeam)
                return false;
        } else {
            if (myTeam !== otherTeam)
                return false;
        }

        if (!aoeRect.isIncludePoint(u.position))
            return false;

        return this.unit.position.getVectorTo(u.position).sqrLength <= this.sqrDistance;
    }

    showInfoMessage(msg, target) {
        this.unit.battle.showServerInfoMessage(msg, target ? target.id : this.unit.id);
    }
}
