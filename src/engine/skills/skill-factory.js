import damageSkills from "./damage-skills";
import healSkills from "./heal-skills";
import supportSkills from "./support-skills";

const skillMap = {};
registerSkills(damageSkills);
registerSkills(healSkills);
registerSkills(supportSkills);

function registerSkills(descriptors) {
    for (let d of descriptors)
        skillMap[d.name] = d.skill;
}

export default {
    createSkill(name, unit, level, props) {
        const ctor = skillMap[name];
        const skill = new ctor(unit, level);
        Object.assign(skill, props);
        return skill;
    },

    getInitialSkillState(skill) {
        return [skill.name, skill.level, skill.key];
    },

    createSkillFromInitialState(state, unit) {
        return this.createSkill(state[0], unit, state[1], {key: state[2]});
    }
};
