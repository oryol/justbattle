export default {
    Ready: 0,
    Cast: 1,
    Prepare: 2,
    Dispose: 3,
    Cooldown: 4
};
