import EngineConstants from "../engine-constants";

import UnitSkill from "./unit-skill";
import SkillTarget from "./skill-target";

class DamageSkill extends UnitSkill {
    constructor(name, unit, level) {
        super(name, unit, level);

        this.skillTarget = SkillTarget.Enemy;
        this.multiplier = 1;
    }

    execute() {
        for (let target of this.targets)
            this.damage(target);
    }

    damage(target) {
        const atk = this.unit.effectiveAttack;
        const def = target.effectiveDefence;
        const damage = this.multiplier * atk * (atk / (atk + def))
            * 10; // temporary for testing


        if (target.healthPoints > 0) {
            target.healthPoints -= damage;
            if (target.healthPoints < 0)
                target.healthPoints = 0;

            target.lastDamageSource = this.unit;
        }

        this.showInfoMessage(`You received damage ${damage}`, target);
        this.showInfoMessage(`You inflicted damage: ${damage}`, this.unit);
    }
}

class SimpleDamageSkill extends DamageSkill {
    constructor(unit, level) {
        super("d1", unit, level);

        this.multiplier = 0.5 + 0.1 * this.level;
        this.setDistance(EngineConstants.skillDistance2);
    }
}

class HighDamageSkill extends DamageSkill {
    constructor(unit, level) {
        super("d2", unit, level);

        this.multiplier = 1 + 0.3 * this.level;
        this.setDistance(EngineConstants.skillDistance3);
        this.castTime = EngineConstants.castTime3;        
        this.cooldownTime = EngineConstants.cooldownTime1;
    }
}

class AoeDamageSkill extends DamageSkill {
    constructor(unit, level) {
        super("d3", unit, level);

        this.multiplier = 0.8 + 0.1 * this.level;
        this.isAoe = true;
        this.setDistance(EngineConstants.skillDistance2);
        this.castTime = EngineConstants.castTime2;
        this.cooldownTime = EngineConstants.cooldownTime2;
    }
}

export default [
    { name: "d1", skill: SimpleDamageSkill },
    { name: "d2", skill: HighDamageSkill },
    { name: "d3", skill: AoeDamageSkill }
];
