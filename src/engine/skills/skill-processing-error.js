export default class SkillProcessingError extends Error {
    constructor(message) {
        super(message);
    }
}