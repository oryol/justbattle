import ArrayUtils from "./common/array-utils";


export default class BattleFormat {
    constructor(name, {playerInTeam = 1, teamCount = 2, timeLimit = 10} = {}) {
        this.playerInTeam = playerInTeam;
        this.teamCount = teamCount;
        this.timeLimit = timeLimit;
        this.name = name;
    }

    toString() {
        return this.name;
    }
}

BattleFormat.get = f => {
    if (f instanceof BattleFormat)
        return f;
        
    if (BattleFormat.formats[f])
        return BattleFormat.formats[f];

    if (BattleFormat.formats.byName[f])
        return BattleFormat.formats.byName[f];

    if (f.name && BattleFormat.formats.byName[f.name])
        return BattleFormat.formats.byName[f.name];

    if (f.format)
        return BattleFormat.get(f.format);

    return null;
};

export const compareFormats = (f1, f2) => ArrayUtils.compare(
    BattleFormat.get(f1), BattleFormat.get(f2), [
        f => f.teamCount,
        f => f.playerInTeam,
        f => f.timeLimit,
        f => f.name
    ]);

BattleFormat.compareFormats = compareFormats;

BattleFormat.formats = {
    _1_1: new BattleFormat("1x1"),
    _4p: new BattleFormat("4p", {teamCount: 4})
    //_8_p: new BattleFormat("8p", {teamCount: 8}),
    //_2_2: new BattleFormat("2x2", {playerInTeam: 2}),
    //_2_2_2: new BattleFormat("2x2x2", {playerInTeam: 2, teamCount: 3}),
    //_3_3: new BattleFormat("3x3", {playerInTeam: 3}),
};

BattleFormat.formats.all = [
    BattleFormat.formats._1_1,
    BattleFormat.formats._4p
    //BattleFormat.formats._8p,
    //BattleFormat.formats._2_2,
    //BattleFormat.formats._2_2_2,
    //BattleFormat.formats._3_3
];

BattleFormat.formats.byName = {}
for (let f of BattleFormat.formats.all)
    BattleFormat.formats.byName[f.name] = f;
