import EventEmitter from "eventemitter3";

import EngineConstants from "./engine-constants";
import BattleUnit from "./battle-unit";
import BattleTeam from "./battle-team";
import BattleMap from "./battle-map";

import ArrayUtils from "./common/array-utils";
import Vector from "./math/vector";

export default class Battle extends EventEmitter {
    constructor(map) {
        super();    

        this.map = map;

        this.units = [];
        this.unitsById = {};
        this.unitsByUser = {};

        this.teams = ArrayUtils.fromRange(map.format.teamCount, i => new BattleTeam(i + 1));

        this.serverMode = false;
        this.timeDiff = 0;

        this.lastUpdateTime = new Date();
        this.lastCommandTime = new Date();

        this.battleStatus = EngineConstants.BattleStatuses.connect;
    }


    addUnit(unit, team) {
        unit.battle = this;
        this.units.push(unit);
        this.unitsById[unit.id] = unit;
        
        if (team) {
            team.units.push(unit);
            unit.team = team;
            unit.indexInTeam = team.units.length - 1;
        }
        
        if (!unit.position)
            this.spawnUnit(unit);
    }


    run() {
        this.stop();
        this.lastUpdateTime = new Date();
        this.timer = setInterval(() => this.update(), 33);

        if (this.serverMode)
            this.setBattleStatus(EngineConstants.BattleStatuses.active);
    }

    stop() {
        if (this.timer)
            clearInterval(this.timer);
    }

    update() {
        if (this.battleStatus != EngineConstants.BattleStatuses.active)
            return;

        const newUpdateTime = new Date();
        const elapsedTime = newUpdateTime - this.lastUpdateTime;

        for (let i = 0; i < 30; i++) {
            if (!this.moveUnits(elapsedTime / 30))
                break;
        }

        this.processSkills(elapsedTime);
        this.processEffects(elapsedTime);
        this.processUnitLifecycle(elapsedTime);

        this.lastUpdateTime = newUpdateTime;
    }

    moveUnits(elapsedTime) {
        for (let u of this.units) {
            u.prepareMove(elapsedTime);
        }

        let moveIterationLimit = 10;
        while (this.checkUnitMoveCollisions() && moveIterationLimit--) {
            // undoMove is performed inside checkeUnitMoveCollisions
        }

        if (moveIterationLimit) {
            //console.error("move iteration limit reached, possible BUG with initial coords or move calculation");
        }
        
        let someUnitMoved = false;
        for (let u of this.units) {
            if (u.move())
                someUnitMoved = true;
        }

        return someUnitMoved;    
    }

    checkUnitMoveCollisions() {
        const collidedUnits = [];

        for (let u1 of this.units) {
            for (let u2 of this.units) {
                if (u1 === u2)
                    continue;

                if (u1.isCollideAfterMove(u2)) {
                    collidedUnits.push(u1);
                    break;
                }
            }
        }

        if (collidedUnits.length === 0)
            return false;

        for (let u of collidedUnits)
            u.undoMove();

        return true;
    }

    processSkills(elapsedTime) {
        for (let unit of this.units) {
            for (let skill of unit.skills) {
                skill.update(elapsedTime);
            }
        }
    }

    processEffects(elapsedTime) {
        for (let unit of this.units) {
            for (let [effectName, effect] of Object.entries(unit.effects)) {
                effect.update(elapsedTime);
            }
        }
    }

    processUnitLifecycle(elapsedTime) {
        for (let u of this.units) {
            if (u.timeToRespawn > 0) {
                u.timeToRespawn -= elapsedTime;
                if (u.timeToRespawn <= 0)
                    this.spawnUnit(u);
                
                continue;
            }

            if (!u.isInBattle)
                continue;

            if (u.healthPoints === 0)
                this.killUnit(u);
        }
    }

    spawnUnit(unit) {
        unit.timeToRespawn = 0;
        unit.isInBattle = false;
        unit.position = this.map.getSpawnPoint(unit);
        unit.updateRect();
        unit.healthPoints = unit.maxHealthPoints;
        unit.lastDamageSource = null;
    }

    killUnit(unit) {
        unit.isInBattle = false;
        unit.timeToRespawn = EngineConstants.respawnTime;

        if (unit.currentSkill)
            unit.currentSkill.reset();

        unit.currentSkill = null;
        unit.skillTarget = null;

        unit.moveDirection = null;
        unit.moveTarget = null;
        unit.moveVector = null;

        unit.statistics.deaths++;
        unit.team.statistics.deaths++;
        
        if (unit.lastDamageSource) {
            unit.lastDamageSource.statistics.kills++;
            unit.lastDamageSource.team.statistics.kills++;
        }

        for (let u of this.units) {
            if (u.skillTarget === unit)
                u.skillTarget = null;
        }
    }


    processServerMessage(message) {
        if (message.i) {
            this.showServerInfoMessage(message.i);
            return;
        }

        if (message.w) {
            this.updateWorldState(message.w);
            return;
        }

        if (message.s) {
            this.setBattleStatus(message.s);
            return;
        }

        if (message.iw) {
            this.loadInitialWorldState(message.iw);
            return;
        }
    }

    processClientMessage(unitId, message) {
        const now = new Date();
        const latency = Math.abs(now - message.t);
        if (latency > 300) {
            console.warn(`Unit ${unitId} message timeout (${latency})`);
            return;
        }

        if (message.c) {
            this.invokePlayerCommand(unitId, message.c);
            return;
        }
    }


    invokePlayerCommand(unitId, command) {
        if (this.serverMode) {
            const unit = this.unitsById[unitId];
            if (unit.timeToRespawn > 0)
                return;

            unit.isInBattle = true;
            
            if (command.md !== undefined)
                unit.setMoveDirection(Vector.fromArray(command.md));

            if (command.mt !== undefined)
                unit.setMoveTarget(Point.fromArray(command.mt));

            if (command.st !== undefined)
                unit.skillTarget = this.unitsById[command.st];

            if (command.s !== undefined) {
                unit.setMoveTarget(null);
                unit.setMoveDirection(null);
                unit.skills[command.s].use();                
            }
        }

        this.emitMessageEvent({c: command}, "player");
    }

    showServerInfoMessage(message, unitId) {
        this.emitMessageEvent({i: message}, "server");
        this.emit("infoMessage", message, unitId);
    }

    setBattleStatus(status) {
        this.battleStatus = status;
        this.emit("statusChanged");
        this.emitMessageEvent({s: status}, "server");
    }

    loadInitialWorldState(state) {
        console.log("Load initial state");
        console.log(state);
        
        this.map = BattleMap.getMapByName(state.m);

        for (let u of state.u) {
            const unit = BattleUnit.createFromInitialState(u, this);
            this.addUnit(unit);
        }
    }

    sendInitialWorldState(unitId) {
        const world = {
            m: this.map.name,
            u: this.units.map(u => u.getInitialState())
        };

        this.emitMessageEvent({iw: world}, "server", unitId);
        this.emit("worldUpdate");
    }

    updateWorldState(state) {
        for (let unitState of state.u) {
            const unit = this.unitsById[unitState.id];            
            unit.updateFromState(unitState);
        }

        this.lastUpdateTime = new Date();
        this.emit("worldUpdate");
    }

    sendWorldState() {
        if (this.battleStatus !== EngineConstants.BattleStatuses.active)
            return;

        const world = {
            u: this.units.map(u => u.getState())
        };

        this.emitMessageEvent({w: world}, "server");
    }


    emitMessageEvent(message, side, unitId) {
        message.t = +new Date() + this.timeDiff;
        this.emit(side + "Message", message, unitId);
    }
}
