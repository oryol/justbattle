import Auth0Lock from "auth0-lock";
import axios from "axios";
import EventEmitter from "eventemitter3";

const clientId = "cbnMNjfyTlVs9L2LHgB3n9h5P8N4huGj";
const authDomain = "oryol.eu.auth0.com";
const appHost = "http://localhost:3000/";

class Auth extends EventEmitter {
    constructor() {
        super();

        this.lock = new Auth0Lock(clientId, authDomain, {
            allowedConnections: ["google-oauth2", "facebook"],
            auth: {
                responseType: "token",
                params: {
                    audience: appHost + "api/",
                    scope: "openid email profile user_metadata"
                }
            }
        });

        this.profile = null;
        this.isAuthenticated = null;

        this.lock.on("authenticated", authResult => this.getUserInfo(authResult.accessToken));
    }

    
    get accessToken() {
        return localStorage.getItem("accessToken");
    }

    set accessToken(value) {
        if (value) {
            localStorage.setItem("accessToken", value);
        } else {
            localStorage.removeItem("accessToken");
        }
    }


    check() {
        const accessToken = this.accessToken;
        if (!accessToken) {
            this.clearAuthInfo();
            return Promise.resolve();
        }
        
        return new Promise(resolve => {
            this.once("stateChanged", resolve);
            this.getUserInfo(accessToken);
        });
    }

    show() {
        this.lock.show();
    }

    logout() {
        this.clearAuthInfo();
        window.location.href = `https://${authDomain}/v2/logout?returnTo=${encodeURIComponent(appHost)}&client_id=${clientId}`;
    }


    getUserInfo(accessToken) {
        this.lock.getUserInfo(accessToken, (err, profile) => {
            if (err) {
                this.clearAuthInfo();
            } else {
                this.saveAuthInfo(accessToken, profile);
            }
        });
    }

    clearAuthInfo() {
        this.accessToken = null;
        this.profile = null;
        this.isAuthenticated = false;

        axios.defaults.headers.common["Authorization"] = null;

        this.emit("stateChanged");
    }    

    saveAuthInfo(accessToken, profile) {
        this.accessToken = accessToken;
        this.profile = profile;
        this.isAuthenticated = true;

        axios.defaults.headers.common["Authorization"] = `Bearer ${accessToken}`;

        this.emit("stateChanged");
    }    
}

export default new Auth();
