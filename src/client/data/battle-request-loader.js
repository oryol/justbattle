import axios from "axios";

import BattleFormat from "../../engine/battle-format";

export default class BattleRequestLoader {
    constructor(unitId) {
        this.unitId = unitId;
        this.reqsByFormatJson = null;
        this.battleFormats = battleFormats.map(f => ({ request: null, ...f }));
    }

    async loadRequests() {
        const reqs = (await axios.get(`/api/units/${this.unitId}/battle-requests`)).data;

        const reqsByFormat = {};
        for (let req of reqs)
            reqsByFormat[req.format] = req;

        const reqsByFormatJson = JSON.stringify(reqsByFormat);
        if (reqsByFormatJson === this.reqsByFormatJson)
            return false;

        this.reqsByFormatJson = reqsByFormatJson;

        for (let format of this.battleFormats) {
            const request = reqsByFormat[format.name] || null;
            format.request = request;
        }

        return true;
    }
}

const battleFormats = BattleFormat.formats.all
    .map(f => ({ name: f.name }))
    .sort(BattleFormat.compareFormats);
