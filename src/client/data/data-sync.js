import EventEmitter from "eventemitter3";
import axios from "axios";

import BattleRequestLoader from "./battle-request-loader";

class DataSync extends EventEmitter {
    constructor() {
        super();

        this.currentBattleId = null;
        this.battleRequestLoaders = [];        
    }

    getBattleFormats(unitId) {
        const loader = this.battleRequestLoaders.find(l => l.unitId === unitId);
        return (loader && loader.battleFormats) ? loader.battleFormats : [];
    }

    async start() {
        if (this.active)
            return;
        
        this.active = true;
        await Promise.all([this.syncCurrentBattleId(), this.syncBattleRequests()]);
    }

    stop() {
        this.active = false;
    }

    addUnit(unitId) {
        if (this.battleRequestLoaders.find(l => l.unitId === unitId))
            return;

        this.battleRequestLoaders.push(new BattleRequestLoader(unitId));
    }

    removeUnit(unitId) {
        this.battleRequestLoaders = this.battleRequestLoaders.filter(l => l.unitId !== unitId);
    }

    async syncCurrentBattleId() {
        if (this.battleIdTimer)
            clearTimeout(this.battleIdTimer);

        if (!this.active)
            return;
        
        try {
            const newBattleId = (await axios.get("/api/battles/current")).data;

            if (newBattleId !== this.currentBattleId) {
                this.currentBattleId = newBattleId;
                this.emit("currentBattleIdChanged", newBattleId);
            }
        } catch (err) {}

        const nextSyncInterval = (this.currentBattleId ? 60 : 1) * 1000;
        this.battleIdTimer = setTimeout(() => this.syncCurrentBattleId(), nextSyncInterval);
    }

    async syncBattleRequests() {
        if (this.battleRequestsTimer)
            clearTimeout(this.battleRequestsTimer);

        if (!this.active)
            return;

        let hasChanges = false;

        try {
            const loadResults = await Promise.all(this.battleRequestLoaders.map(l => l.loadRequests()));
            hasChanges = loadResults.some(r => r);
            if (hasChanges)
                this.emit("battleRequestsChanged");
        } catch (err) {}

        const nextSyncInterval = 5000;
        this.battleRequestsTimer = setTimeout(() => this.syncBattleRequests(), nextSyncInterval);
    }
}

export default new DataSync();
