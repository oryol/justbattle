import Phaser from "phaser";

import EngineConstants from "../../engine/engine-constants";

export default class UnitBody extends Phaser.Group {
    constructor(unit, myUnit, game, parent) {
        super(game, parent, `Unit-Body-${unit.id}`);

        this.unit = unit;
        this.myUnit = myUnit;

        this.childType = Phaser.Image;

        this.bg = this.create(0, 0, "unit-body-bg");
        this.bg.anchor.setTo(0.5, 0.5);
        this.bg.inputEnabled = true;
        this.bg.events.onInputDown.add(() => unit.battle.inputHandler.setSkillTarget(unit));

        this.focus = this.create(0, 0, "unit-body-focus");
        this.focus.anchor.setTo(0.5, 0.5);
        this.focus.visible = false;

        this.healthBar = this.create(-19, 14, "unit-body-health-bar");
        this.healthBar.anchor.setTo(0, 0.5);
    }

    update() {
        const p = this.unit.position;
        this.position.setTo(p.x, p.y);
        this.focus.visible = this.unit === this.myUnit.skillTarget;
        this.healthBar.width = (this.unit.healthPoints / this.unit.maxHealthPoints) * 38;
    }
}

