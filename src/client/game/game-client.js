import SockJS from "sockjs-client";

import Battle from "../../engine/battle";
import BattleMap from "../../engine/battle-map";

import Game from "./game.js"
import Auth from "../auth";


export default class GameClient {
    constructor(element, battleId) {
        this.element = element;
        this.battleId = battleId;

        this.battle = null;
        this.timeDiff = null;

        this.sockjs = new SockJS(`/sockets/battle/${this.battleId}`);
        this.sockjs.onopen = () => this.onSocketConnected();
        this.sockjs.onmessage = e => this.onSocketMessage(e.data);
        this.sockjs.onclose = () => this.sockjs = null;
    }

    destroy() {
        if (this.sockjs)
            this.sockjs.close();

        if (this.battle)
            this.battle.stop();

        if (this.game)
            this.game.destroy();
    }

    onSocketConnected() {
        this.requestSendTime = +new Date();
        this.sockjs.send("");
    }

    onSocketMessage(msg) {
        if (this.timeDiff === null) {
            this.syncTime(+msg);
        } else if (!this.battle) {
            this.createBattle(JSON.parse(msg));
        } else {
            this.battle.processServerMessage(JSON.parse(msg));
        }
    }

    syncTime(serverTime) {
        const responseReceivedTime = +new Date();
        const currentServerTime = serverTime + (responseReceivedTime - this.requestSendTime) / 2;
        this.timeDiff = currentServerTime - responseReceivedTime;
        console.log(`Time diff: ${this.timeDiff}`);

        this.sockjs.send(Auth.accessToken);
    }

    createBattle(battleInfo) {        
        const map = BattleMap.getMapByName(battleInfo.map);
        console.log(`Map [${battleInfo.map}] :: ${map}`);
        
        this.battle = new Battle(map);
        this.battle.timeDiff = this.timeDiff;        

        this.battle.on("playerMessage", msg => {
            this.sockjs.send(JSON.stringify(msg));
        });

        this.battle.run();

        this.battle.once("worldUpdate", () => {
            this.myUnit = this.battle.unitsById[battleInfo.unitId];
            this.game = new Game(this.element, this.battle, this.myUnit);
        });
    }
}
