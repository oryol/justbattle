import Phaser from "phaser";

import EngineConstants from "../../engine/engine-constants";
import SkillStage from "../../engine/skills/skill-stage";


export default class SkillProgress extends Phaser.Group {
    constructor(unit, myUnit, game, parent) {
        super(game, parent, `Skill-Progress-${unit.id}`);

        this.unit = unit;
        this.myUnit = myUnit;

        this.childType = Phaser.Image;
        this.visible = false;

        this.graphics = new Phaser.Graphics(this.game, 0, 0);
        this.add(this.graphics);

        this.textBlock = new Phaser.Text(this.game,
            EngineConstants.unitSize / 2 + 20, -6,
            "", {fontSize: "12px"});
        this.add(this.textBlock);
    }

    update() {
        const skill = this.unit.currentSkill;
        if (!skill || skill.stage === SkillStage.Dispose) {
            this.visible = false;
            return;
        }

        this.visible = true;

        this.x = this.unit.position.x;
        this.y = this.unit.position.y;

        this.textBlock.text = skill.name;
        this.drawSkillProgress(skill);
    }

    drawSkillProgress(skill) {
        this.graphics.clear();
        this.graphics.lineStyle(5, 0x222222, 0.7);

        const progress = Math.max(Math.min((skill.stageTime - skill.stageRemainedTime) / skill.stageTime, 1), 0);            

        if (skill.stage === SkillStage.Cast) {
            console.log("draw cast");
            // draw cast
            return;
        }

        if (skill.isAoe) {
            console.log("draw aoe");
            // draw aoe
            return;
        }

        if (skill.singleTarget === this.unit) {
            console.log("draw self skill");
            // draw self skill
            return;
        }

        // draw single target skill
        const myPos = this.unit.position;
        const targetPos = skill.singleTarget.position;
        const targetV = myPos.getVectorTo(targetPos).multiple(progress);
        
        this.graphics.moveTo(0, 0);
        this.graphics.lineTo(targetV.x, targetV.y);
    }
}
