import "pixi";
import "p2";
import Phaser from "phaser";

import InputHandler from "./input-handler";
import UnitBody from "./unit-body";
import SkillProgress from "./skill-progress";
import MiniMap from "./mini-map";


export default class Game extends Phaser.Game {
    constructor(element, battle, myUnit) {
        super(900, 600, Phaser.AUTO, element);
        this.state.add("Main", new MainGameState(battle, myUnit), true);
    }
}

class MainGameState extends Phaser.State {
    constructor(battle, myUnit) {
        super();

        this.battle = battle;
        this.myUnit = myUnit;            
    }


    preload() {
        this.load.path = "/game-assets/";

        const images = [
            "map-bg",
            "unit-body-bg", "unit-body-focus", "unit-body-health-bar",
            "mini-map-bg", "mini-map-unit-me", "mini-map-unit-ally", "mini-map-unit-enemy"
        ];

        for (let key of images)
            this.load.image(key);
    }

    create() {
        this.stage.disableVisibilityChange = true;
        this.stage.backgroundColor = "#AAAAAA";

        const emptySpaceX = this.game.width / 2 + 5;
        const emptSpaceY = this.game.height / 2 + 5;

        const mapWidth = this.battle.map.width;
        const mapHeight = this.battle.map.height;

        this.world.bounds = new Phaser.Rectangle(-emptySpaceX, -emptSpaceY, mapWidth + emptySpaceX, mapHeight + emptSpaceY);
        this.camera.bounds = null;

        const bg = this.add.image(0, 0, "map-bg");
        bg.width = mapWidth;
        bg.height = mapHeight;

        const unitBodies = this.add.group();
        const skillProgresses = this.add.group();

        for (let unit of this.battle.units) {
            const body = new UnitBody(unit, this.myUnit, this.game, unitBodies);
            unitBodies.add(body);

            const skillProgress = new SkillProgress(unit, this.myUnit, this.game, skillProgresses);
            skillProgresses.add(skillProgress);

            unit.gameObjects = { body, skillProgress };
        }

        this.miniMap = new MiniMap(this.battle, this.myUnit, this.game);
        this.battle.inputHandler = new InputHandler(this.battle, this.myUnit, this.input);
    }

    update() {
        for (let unit of this.battle.units) {
            unit.gameObjects.body.update();
            unit.gameObjects.skillProgress.update();
        }

        this.miniMap.update();
        this.camera.focusOnXY(this.myUnit.position.x, this.myUnit.position.y);
    }
}