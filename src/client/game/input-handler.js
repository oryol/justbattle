import Phaser from "phaser";

import EngineConstants from "../../engine/engine-constants";

export default class InputHandler {
    constructor(battle, myUnit, input) {
        this.battle = battle;        
        this.myUnit = myUnit;
        this.input = input;
        
        this.upKey = input.keyboard.addKey(Phaser.Keyboard.W);
        this.downKey = input.keyboard.addKey(Phaser.Keyboard.S);
        this.leftKey = input.keyboard.addKey(Phaser.Keyboard.A);
        this.rightKey = input.keyboard.addKey(Phaser.Keyboard.D);        

        const moveKeys = [this.upKey, this.downKey, this.leftKey, this.rightKey];
        for (let mk of moveKeys) {
            mk.onDown.add(() => this.changeMoveDirection());
            mk.onUp.add(() => this.changeMoveDirection());
        }

        this.tabKey = input.keyboard.addKey(Phaser.Keyboard.TAB);
        this.tabKey.onDown.add(() => this.focusNextTarget());

        for (let [skillIdx, skill] of this.myUnit.skills.entries())
            this.bindSkill(skillIdx, skill.key);
    }

    bindSkill(skillIdx, key) {
        const k = parseKey(key);
        if (k) {
            const phaserKey = this.input.keyboard.addKey(k.keyCode);
            phaserKey.onDown.add(() => {
                if (phaserKey.altKey !== k.alt)
                    return;

                if (phaserKey.ctrlKey !== k.control)
                    return;

                if (phaserKey.shiftKey !== k.shift)
                    return;

                this.invokePlayerCommand({s: skillIdx});
            });
        }
    }

    changeMoveDirection() {
        const dx = (this.leftKey.isDown ? -1 : 0) + (this.rightKey.isDown ? 1 : 0);
        const dy = (this.upKey.isDown ? -1 : 0) + (this.downKey.isDown ? 1 : 0);        
        const md = (dx === 0 && dy === 0) ? null : [dx, dy];
        this.invokePlayerCommand({md});
    }

    setSkillTarget(unit) {
        this.invokePlayerCommand({st: unit ? unit.id : null});
    }

    focusNextTarget() {
        const currentTarget = this.myUnit.skillTarget || this.myUnit;
        const currentTargetIdx = this.battle.units.indexOf(currentTarget);
        
        let nextTargetIdx = (currentTargetIdx + 1) % this.battle.units.length;
        while (nextTargetIdx !== currentTargetIdx) {
            const nextTarget = this.battle.units[nextTargetIdx];
            if (this.myUnit.position.getVectorTo(nextTarget.position).length <= EngineConstants.skillDistance3) {
                this.setSkillTarget(nextTarget);
                return;
            }

            nextTargetIdx = (nextTargetIdx + 1) % this.battle.units.length;            
        }

        this.setSkillTarget(null);
    }

    invokePlayerCommand(cmd) {
        this.battle.invokePlayerCommand(this.myUnit.id, cmd);
    }
}

function parseKey(key) {
    const result = {alt: false, control: false, shift: false};
    
    let m = key.charAt(0);
    if (m === "c") {
        result.control = true;
    } else if (m === "a") {
        result.alt = true;
    } else if (m === "s") {
        result.shift = true;
    } else {
        m = null;
    }

    if (m)
        key = key.substr(1);

    const keyCode = Phaser.KeyCode[key];
    if (!keyCode)
        return null;

    result.keyCode = keyCode;
    return result;
}