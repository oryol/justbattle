import Phaser from "phaser";

import EngineConstants from "../../engine/engine-constants";

const size = 170;

export default class MiniMap extends Phaser.Group {
    constructor(battle, myUnit, game, parent) {
        super(game, parent, `Mini-Map`);

        this.battle = battle;
        this.myUnit = myUnit;

        this.childType = Phaser.Image;
        this.fixedToCamera = true;        

        const mapW = battle.map.width;
        const mapH = battle.map.height;
        const ratioW = mapW / size;
        const ratioH = mapH / size;
        this.ratio = Math.max(ratioW, ratioH);

        this.bg = this.create(0, 0, "mini-map-bg");
        this.bg.width = mapW / this.ratio;
        this.bg.height = mapH / this.ratio;

        this.cameraOffset.setTo(0, game.height - this.bg.height);
    }

    update() {
        for (let unit of this.battle.units) {
            const unitImg = this.getUnitImage(unit);
            unitImg.x = unit.position.x / this.ratio;
            unitImg.y = unit.position.y / this.ratio;
        }
    }

    getUnitImage(unit) {
        const gameObjects = unit.gameObjects;
        if (gameObjects.miniMapUnit)
            return gameObjects.miniMapUnit;
        
        const imageKey = "mini-map-unit-" + this.getImageKey(unit);
        const img = gameObjects.miniMapUnit = this.create(0, 0, imageKey);
        img.anchor.setTo(0.5, 0.5);
        
        return img;
    }

    getImageKey(unit) {
        if (unit === this.myUnit)
            return "me";

        if (unit.team.id === this.myUnit.team.id)
            return "ally";

        return "enemy";
    }
}