import Vue from "vue";
import VueRouter from "vue-router";
import Vuetify from "vuetify";

import LandingPage from "./components/landing-page.vue";
import PlayerRootPage from "./components/player-root-page.vue";
import PlayerDashboardPage from "./components/player-dashboard-page.vue";
import AccountSettingsPage from "./components/account-settings-page.vue";
import UnitDashboardPage from "./components/unit-dashboard-page.vue";
import BattlePage from "./components/battle-page.vue";

import Auth from "./auth";
import DataSync from "./data/data-sync";

import "./styles/main.styl";


Vue.use(VueRouter);
Vue.use(Vuetify);

const router = new VueRouter({
    mode: "hash", // TODO: switch to "history"
    routes: [
        {
            path: "/",
            name: "Landing",
            component: LandingPage
        },
        {
            path: "/player",
            component: PlayerRootPage,
            meta: { needAuth: true },
            children: [
                {
                    path: "",
                    name: "PlayerDashboard",
                    component: PlayerDashboardPage
                },
                {
                    path: "account",
                    name: "AccountSettings",
                    component: AccountSettingsPage
                },
                {
                    path: "unit/:unitId",
                    name: "UnitDashboard",
                    component: UnitDashboardPage
                },
                {
                    path: "battle",
                    name: "Battle",
                    component: BattlePage
                }
            ]
        }
    ]
});

const getRootPage = () => ({ name: Auth.isAuthenticated ? "PlayerDashboard" : "Landing" });

router.beforeEach(async (to, from, next) => {
    if (to.matched.length === 0)
        return Auth.once("stateChanged", () => next(getRootPage()));

    const needAuth = to.matched.some(r => r.meta.needAuth);
    if (needAuth === Auth.isAuthenticated)
        return next();

    next(getRootPage());
});

async function updateDataSync() {
    if (Auth.isAuthenticated) {        
        await DataSync.start();
    } else {
        DataSync.stop();
    }
}

(async function() {
    await Auth.check();
    await updateDataSync();
    Auth.on("stateChanged", updateDataSync);
    new Vue({ router }).$mount("#app");
})();
