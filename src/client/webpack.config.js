const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const env = process.env.NODE_ENV;
const isProd = env === "production" || env == "prod";

const phaserModule = path.join(__dirname, "../node_modules/phaser");
const phaser = path.join(phaserModule, 'build/custom/phaser-split.js');
const pixi = path.join(phaserModule, 'build/custom/pixi.js');
const p2 = path.join(phaserModule, 'build/custom/p2.js');

const babelLoader = {
    loader: "babel-loader",
    options: {
        presets: [
            ["env", {
                targets: {
                    browsers: ["last 2 versions"],
                    uglify: true
                }
            }]
        ],
        plugins: [
            "transform-object-rest-spread"
        ]
    }
};

module.exports = {
    entry: {
        app: ["babel-polyfill", path.resolve(__dirname, "index.js")],
        vendor: [
            "pixi", "p2", "phaser",
            "sockjs-client",
            "vue", "vue-router", "vuetify",
            "auth0-lock",
            "axios", "eventemitter3"
        ]
    },
    output: {
        path: path.resolve(__dirname, "../bundle"),
        filename: "[name].js",
        publicPath: "/bundle/"
    },
    devtool: "source-map", // "cheap-module-eval-source-map",
    module: {
        rules: [
            {
                test: /pixi\.js$/,
                use: {
                    loader: "expose-loader",
                    options: "PIXI"
                }
            },
            {
                test: /phaser-split\.js$/,
                use: {
                    loader: "expose-loader",
                    options: "Phaser"
                }
            },
            {
                test: /p2\.js$/,
                use: {
                    loader: "expose-loader",
                    options: "p2"
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: babelLoader
            },
            {
                test: /\.vue$/,
                use: {
                    loader: "vue-loader",
                    options: {
                        loaders: {
                            js: babelLoader
                        }
                    }
                }
            },
            {
                test :/\.styl$/,
                use: ExtractTextPlugin.extract({
                    use: ["css-loader", "stylus-loader"]
                })
            }
        ]
    },
    resolve: {
        alias: {
            phaser, pixi, p2,
            vue: "vue/dist/vue.esm"
        }
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": { NODE_ENV: env}
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: "vendor"
        }),
        new ExtractTextPlugin("styles.css")
    ]
};
