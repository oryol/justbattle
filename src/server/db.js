import mongodb from "mongodb";

import {DB_URL} from "./env-config";

class Db {
    async connect() {
        if (this.db)
            throw new Error("Already connected");

        this.db = await mongodb.MongoClient.connect(DB_URL);

        this.users = this.db.collection("users");
        this.units = this.db.collection("units");
        this.battleRequests = this.db.collection("battleRequests");
        this.battles = this.db.collection("battles");

        await Promise.all([
            this.units.createIndex({"user_id": 1}),
            this.battleRequests.createIndex({"format": 1, "unit.level": 1}),
            this.battles.createIndex({"host": 1, "started": 1, "creationTime": 1})
        ]);

        process.on("exit", () => this.close());
        process.on("SIGINT", () => this.close());
    }

    async close() {
        await this.db.close();
    }

    toObjectId(id) {
        return new mongodb.ObjectId(id);
    }
}

const db = new Db();
export default db;

export async function createDbMiddleware() {
    await db.connect();

    return (req, res, next) => {
        req.db = db;
        next();
    };
};
