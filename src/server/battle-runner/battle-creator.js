import db from "../db";

import ArrayUtils from "../../engine/common/array-utils";
import BattleFormat from "../../engine/battle-format";

import BattleStatus from "./battle-status";
import {timeout} from "./utils";

export default class BattleCreator {
    constructor(format) {
        this.format = BattleFormat.formats.byName[format];
        if (!this.format)
            throw new Error(`Invalid format: ${format}`);
        
        this.teams = ArrayUtils.fromRange(this.format.teamCount, i => []);
        this.users = [];
        this.units = [];
    }

    get isFull() {
        return this.teams.every(t => t.length === this.format.playerInTeam);
    }


    processRequest(request) {
        if (request.unit)
            return this.processUnitRequest(request.unit);

        throw new Error("Unsupported request")
    }
    
    async processUnitRequest(unit) {
        if (!await this.addUserToBattle(unit.user_id))
            return false;
        
        const team = this.teams
            .filter(t => t.length < this.format.playerInTeam)
            .sort((t1, t2) => t1.length - t2.length)
            [0];
        
        const teamUnit = { unitId: unit.id, userId: unit.user_id };
        team.push(teamUnit);

        this.users.push(teamUnit.userId);
        this.units.push(teamUnit.unitId);

        return true;
    }

    async addUserToBattle(userId) {
        const updateResult = await db.users.updateOne(
            { _id: userId, battleStatus: BattleStatus.NoBattle },
            { $set: { battleStatus: BattleStatus.PossibleBattle } });
        
        return updateResult.modifiedCount > 0;
    }


    async completeBattleCreation() {
        const host = "X"; // todo: choose host

        const units = await this.loadUnits();        
        const teams = [];
        const userUnits = {};
        let unitId = 1;

        for (let t of this.teams) {
            const team = [];
            teams.push(team);

            for (let u of t) {
                const unit = {host_unitId: unitId++, ...units[u.unitId]};
                team.push(unit);
                userUnits[u.userId] = unit.host_unitId;
            }
        }

        const battleInsertResult = await db.battles.insertOne({
            creationTime: new Date(),
            format: this.format.name,
            started: false,
            host, teams, userUnits
        });

        const battleId = battleInsertResult.insertedId;

        await db.battleRequests.deleteMany(
            { "unit.user_id": { $in: this.users } });

        await db.users.updateMany(
            { _id: { $in: this.users } },
            { $set: { battleStatus: BattleStatus.ActiveBattle, currentBattle_id: null } });
    }

    async loadUnits() {
        const r = {};
        const l = await db.units.find({ _id: { $in: this.units } }).toArray();

        for (let u of l)
            r[u._id] = u;

        return r;            
    }


    async undoBattleCreation() {
        await db.users.updateMany(
            { _id: { $in: this.users } },
            { $set: { battleStatus: BattleStatus.NoBattle } });
    }
}