import BattleFormat from "../../engine/battle-format";

import db from "../db";

import BattleCreator from "./battle-creator";
import {timeout} from "./utils";

export default class BattleRequestProcessor {
    async run() {
        try {
            await db.connect();

            while (true) {
                await this.createBattles();
                await timeout(500);
            }
        } catch (e) {
            console.error(e);
        }

        await db.close();
    }

    async createBattles() {
        for (let format of BattleFormat.formats.all)
            await this.createBattles_format(format.name);
    }

    async createBattles_format(format) {        
        const levels = (await db.battleRequests
            .aggregate([
                { $match: { format } },
                { $group: {
                    _id: null,
                    minLevel: {$min: "$unit.level"},
                    maxLevel: {$max: "$unit.level"}
                }}])
            .toArray())
            [0];

        if (!levels)
            return;
        
        const step = 5;
        const stepsPerBattle = 2;

        const minLevel = Math.floor((levels.minLevel - 1) / step) * step + 1;
        const maxLevel = Math.ceil(levels.maxLevel / step) * step;

        for (let level = minLevel; level <= maxLevel; level += step)
            await this.createBattles_format_level(format, level, level + step * stepsPerBattle - 1);        
    }

    async createBattles_format_level(format, minLevel, maxLevel) {
        while (true) {
            const isCreated = await this.createBattle(format, minLevel, maxLevel);
            if (!isCreated) {
                return;
            }
        }
    }

    async createBattle(format, minLevel, maxLevel) {
        const battleRequests = await db.battleRequests
            .find({ format, "unit.level": { $gte: minLevel, $lte: maxLevel } })
            .sort({ creationTime: 1 })
            .limit(30)
            .toArray();
            
        shuffle(battleRequests);
        const battleCreator = new BattleCreator(format);

        for (let req of battleRequests) {
            if (!await battleCreator.processRequest(req))
                continue;

            if (battleCreator.isFull) {
                await battleCreator.completeBattleCreation();
                return true;
            }
        }

        await battleCreator.undoBattleCreation();

        return false;
    }
}

function shuffle(arr, step = 3) {
    for (let i = arr.length; i; i--) {
        let j = Math.max(i - 1 - Math.floor(Math.random() * step), 0);
        [arr[i - 1], arr[j]] = [arr[j], arr[i - 1]];
    }
}


if (require.main === module)
    new BattleRequestProcessor().run();
