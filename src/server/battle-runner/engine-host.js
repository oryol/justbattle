import Battle from "../../engine/battle";
import BattleMap from "../../engine/battle-map";
import BattleUnit from "../../engine/battle-unit";

import {ENGINE_HOST_NAME} from "../env-config";
import db from "../db";

import {timeout} from "./utils";
import BattleStatus from "./battle-status";


const battles = {};


async function startBattles_worker() {
    await removeZombieBattles();

    while (true) {
        try {        
            const dbBattles = await db.battles
                .find({ host: ENGINE_HOST_NAME, started: false })
                .sort({ creationTime: 1 })
                .limit(5)
                .toArray();

            for (let dbBattle of dbBattles)
                await startBattle(dbBattle);

            await timeout(2000);        
        } catch (err) {
            console.error(err);
        }
    }
}

async function removeZombieBattles() {
    const battles = await db.battles
        .find({ host: ENGINE_HOST_NAME, started: true })
        .project({ userUnits: 1 })
        .toArray();

    const userIds = new Set();
    const battleIds = new Set();
    
    for (let battle of battles) {
        battleIds.add(battle._id);

        for (let userId of Object.keys(battle.userUnits))
            userIds.add(userId);
    }

    await db.users.updateMany(
        { _id: { $in: Array.from(userIds) } },
        { $set: { battleStatus: BattleStatus.NoBattle, currentBattle_id: null } });

    await db.battles.deleteMany({ _id: { $in: Array.from(battleIds) } });
}

async function startBattle(dbBattle) {
    const map = BattleMap.getMapForFormat(dbBattle.format);
    const battle = new Battle(map);
    battle.serverMode = true;
    battle.id = `${ENGINE_HOST_NAME}_${dbBattle._id.toString()}`;
    battle.db_id = dbBattle._id;

    for (let teamIdx = 0; teamIdx < dbBattle.teams.length; teamIdx++) {
        const dbTeam = dbBattle.teams[teamIdx];
        const team = battle.teams[teamIdx];

        for (let dbUnit of dbTeam) {
            const unit = BattleUnit.createFromDbUnit(dbUnit);
            battle.addUnit(unit, team);            
        }
    }

    const userIds = Object.keys(dbBattle.userUnits);
    for (let userId of userIds)
        battle.unitsByUser[userId] = dbBattle.userUnits[userId];

    battles[battle.id] = battle;

    await db.battles.updateOne(
        { _id: dbBattle._id },
        { $set: { started: true }});

    await db.users.updateMany(
        { _id: { $in: userIds } },
        { $set: { currentBattle_id: dbBattle._id }});

    setTimeout(() => runBattle(battle), 2000);
}

async function runBattle(battle) {
    battle.run();
    await timeout(battle.map.format.timeLimit * 60 * 1000);
    
    battle.stop();
    delete battles[battle.id];

    for (let unit of battle.units) {
        const {kills, deaths} = unit.statistics;
        battle.showServerInfoMessage(`Your result: [kills: ${kills}, deaths: ${deaths}]`, unit.id);
    }

    await db.battles.deleteOne({ _id: battle.db_id });

    // TODO: update user points / battle history
}

async function sendBattleStates_worker() {    
    while (true) {
        try {
            for (let battleId of Object.keys(battles))
                battles[battleId].sendWorldState();
            
            await timeout(100);
        } catch (err) {
            console.error(err);
        }
    }
}


export default {
    getBattle: id => {
        console.log(`Battle ids: ${Object.keys(battles)}`);
        return battles[id];
    },
    
    run() {
        if (!ENGINE_HOST_NAME)
            return;

        startBattles_worker();
        sendBattleStates_worker();
    }
};
