import url from "url";


export const WEB_HOST = process.env.JUST_BATTLE_WEB_HOST || "localhost";
export const WEB_PORT = process.env.JUST_BATTLE_WEB_PORT || 3000;
export const WEB_SSL = process.env.JUST_BATTLE_WEB_SSL === 1;
export const WEB_ROOT_URL = buildUrl(WEB_HOST, WEB_PORT, WEB_SSL);

export const ENGINE_HOST_NAME = process.env.JUST_BATTLE_ENGINE_HOST_NAME || "X";

export const DB_URL = process.env.JUST_BATTLE_DB_URL || "mongodb://localhost/just-battle";

export const AUTH0_SERVICE_HOST = process.env.JUST_BATTLE_AUTH_HOST = "oryol.eu.auth0.com";
export const AUTH0_SERVICE_URL = buildUrl(AUTH0_SERVICE_HOST, undefined, true);

const env = process.env.NODE_ENV
export const ENV_IS_PROD = env === "production" || env === "prod";


function buildUrl(hostname, port, ssl) {
    let u = {hostname, protocol: ssl ? "https" : "http"};
    
    const defaultPort = ssl ? 443 : 80;
    if (port && port != defaultPort)
        u.port = port;

    return url.format(u);
}
