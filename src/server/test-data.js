import mongodb from "mongodb";

import db from "./db";

import ArrayUtils from "../engine/common/array-utils";
import RandomUtils from "../engine/math/random-utils";


const myUserId = "google-oauth2|104960082982365249556";
const myUnitId = new mongodb.ObjectId("5913186356a6323e30798425");


async function users() {
    await db.users.insertMany(ArrayUtils
        .fromRange(5, i => ({
            _id: `user|${i + 1}`,
            battleStatus: 0,
            unitCount: 1
        })));

    await db.users.updateMany({}, { $set: { currentBattle_id: null }});
}

function units() {
    return db.units.insertMany(ArrayUtils
        .fromRange(6, i => ({
            _id: (i === 0) ? myUnitId : undefined,
            user_id: (i === 0) ? myUserId : `user|${i}`,
            name: `TestUnit${i}`,
            level: 10,
            stats: {
                maxHealthPoints: 10,
                attack: 10,
                defence: 10,
                healPower: 10
            },
            skills: [
                { name: "d1", level: 1, key: "Q" },
                { name: "d2", level: 1, key: "R" },
                { name: "h1", level: 1, key: "E" },
                { name: "h2", level: 1, key: "F" },
                { name: "s1", level: 10, key: "1" },
                { name: "s2", level: 10, key: "2" },
                { name: "s3", level: 10, key: "3" }
            ]
        })));
}

async function battleRequests() {
    const units = await db.units.find({ user_id: { $regex: /^user/ }}).toArray();

    await db.battleRequests.insertMany(units
        .map(u => ArrayUtils
            .fromRange(1, i => ({
                creationTime: new Date() + RandomUtils.intFromRange(-1000, 1000),
                format: "1x1",
                unit: {
                    id: u._id,
                    user_id: u.user_id,
                    level: u.level
                }
            })))
        .reduce((a, b) => a.concat(b)));        
}


(async function() {
    await db.connect();

    if (true) {
        await db.users.drop();
        await db.units.drop();
        await db.battleRequests.drop();
        await db.battles.drop();
    }

    await users();
    await units();
    await battleRequests();

    await db.close();
})();