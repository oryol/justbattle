import sockjs from "sockjs";
import uuid from "uuid";

import EngineHost from "../battle-runner/engine-host";

import {getAuthInfo} from "../auth";
import db from "../db";

const server = sockjs.createServer({ sockjs_url: "http://localhost:3000/bundle/vendor.js" });
const connectionsByClientId = {};

server.on("connection", conn => {
    const params = /sockets\/battle\/(\w+)\//.exec(conn.url);
    const battleId = params[1];
    console.log(`Get battle: ${battleId}`);
    
    const battle = EngineHost.getBattle(battleId);
    if (!battle) {
        conn.close(0, "Battle doesn't exist");
        return;
    }

    const clientId = uuid().replace(/\-/g, "");
    conn.clientId = clientId;
    connectionsByClientId[clientId] = conn;

    if (!battle.clients) {
        battle.clients = [];
        setupBattle(battle);
    }

    battle.clients.push(clientId);

    conn.on("close", () => {
        delete connectionsByClientId[clientId];
    });

    conn.on("data", async msg => {
        if (!conn.timeSync) {
            conn.timeSync = true;
            conn.write(+new Date());
            return;
        }

        if (!conn.authValidated) {            
            const user = await getAuthInfo(msg); // first message is auth token
            conn.authValidated = true;
            conn.unitId = battle.unitsByUser[user.sub];
            
            conn.write(JSON.stringify({
                map: battle.map.name,
                unitId: conn.unitId
            }));

            battle.sendInitialWorldState(conn.unitId);

            return;
        }

        const unitId = conn.unitId;
        if (!unitId) {
            console.warn(`Send command message [${msg}] to connection without unit`);
            return;
        }

        battle.processClientMessage(unitId, JSON.parse(msg));
    });
});

function setupBattle(battle) {
    battle.on("serverMessage", (msg, unitId) => {        
        msg = JSON.stringify(msg);        

        for (let clientId of battle.clients) {
            const conn = connectionsByClientId[clientId];
            if (!conn || conn.readyState !== 1 || conn.authValidated !== true)
                continue;
            
            if (unitId !== undefined && unitId !== conn.unitId)
                continue;

            conn.write(msg);
        }
    });
}

export default {
    configureServer: httpServer => server.installHandlers(httpServer, {prefix: "/sockets/battle/\\w+"})
};
