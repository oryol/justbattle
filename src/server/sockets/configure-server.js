import BattleSockets from "./battle";

export default function configureServer(httpServer) {
    BattleSockets.configureServer(httpServer);
};
