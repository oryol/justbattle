import expressJwt from "express-jwt";
import jwks from "jwks-rsa";
import jwt from "jsonwebtoken"
import promisify from "es6-promisify";

import {AUTH0_SERVICE_URL} from "./env-config";


const jwksOptions = {
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `${AUTH0_SERVICE_URL}/.well-known/jwks.json`
};

const jwtOptions = {
    issuer: `${AUTH0_SERVICE_URL}/`,
    algorithms: ['RS256']
};


const expressJwtMiddleware = expressJwt({
    secret: jwks.expressJwtSecret(jwksOptions),
    ...jwtOptions
});

export function expressAuth(req, res, next) {
    expressJwtMiddleware(req, res, async () => {
        if (!req.user) {
            next();
            return;
        }

        req.userId = req.user.sub;
        await createDbUser(req);
        next();
    });
};

async function createDbUser(req) {
    try {
        await req.db.users.insertOne({ _id: req.userId, battleStatus: 0 });
    } catch (err) {}
}


const jwksClient = jwks(jwksOptions);

async function getAuthTokenImpl(token) {
    const header = (jwt.decode(token, { complete: true }) || {}).header;
    if (!header || header.alg !== "RS256")
        return false;

    const secretKeys = await promisify(jwksClient.getSigningKey, jwksClient)(header.kid);
    const secret = secretKeys.publicKey || secretKeys.rsaPublicKey;

    const dtoken = await promisify(jwt.verify)(token, secret, jwtOptions);
    return dtoken;
}

export async function getAuthInfo(token) {
    try {
        return await getAuthTokenImpl(token);
    } catch (err) {
        console.warn(err);
        return false;
    }
};
