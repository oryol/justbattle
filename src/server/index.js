import express from "express";
import http from "http";
import bodyParser from "body-parser";

import {WEB_PORT} from "./env-config";
import {createDbMiddleware} from "./db";

import routes from "./api/routes";
import configureSockets from "./sockets/configure-server";
import EngineHost from "./battle-runner/engine-host";

(async function run() {
    const db = await createDbMiddleware();

    EngineHost.run();

    const expressApp = express()
        .use(bodyParser.json())
        .use(db)
        .use("/", routes);

    const server = http.createServer(expressApp);
    configureSockets(server);

    server.listen(WEB_PORT, () => console.log(`Server running on port ${WEB_PORT}`));
})();