import express from "express";

const r = express.Router();


r.get("/", async (req, res) => {
    await req.db.collection("test").insert({x: 1, a: "123"});
    res.send("test");
});


export default r;
