import express from "express";
import httpProxy from "http-proxy";
import path from "path";

import {ENV_IS_PROD} from "../env-config";
import {expressAuth} from "../auth";

import testApi from "./test";
import unitsApi from "./units";
import battlesApi from "./battles";


const router = express.Router();


router    
    .use("/api/test", expressAuth, testApi)
    .use("/api/units", expressAuth, unitsApi)
    .use("/api/battles", expressAuth, battlesApi);


if (ENV_IS_PROD) {
    router.use(express.static(path.resolve(__dirname, "../public")));
    router.use("/bundle", express.static(path.resolve(__dirname, "../bundle")));
} else {
    const proxy = httpProxy.createProxyServer();

    const proxyHandler = (req, res) => proxy
        .web(req, res, {target: "http://localhost:3003"});

    router.all("/bundle/*", proxyHandler);
    router.all("/game-assets/*", proxyHandler);
    router.get("/", proxyHandler);
}


export default router;