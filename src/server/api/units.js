import express from "express";

import battleRequestsApi from "./battleRequests";
import ServerConstants from "../server-constants";


const r = express.Router();


const parseUnitId = async (req, res, next) => {
    req.unitId = req.db.toObjectId(req.params.unitId);

    const unitCount = await req.db.units
        .count({ _id: req.unitId, user_id: req.userId });

    if (unitCount === 0) {
        res.status(404).json({ error: "Unit doesn't exist"});
    } else {
        next();
    }
};


r.get("/", async (req, res) => {
    const units = await req.db.units
        .find({ user_id: req.userId })
        .sort({ creationTime: 1 })
        .toArray();

    res.json(units.map(u => ({
        id: u._id,
        name: u.name,
        level: u.level,
        skills: u.skills.map(s => ({
            name: s.name,
            key: s.key
        }))
    })));
});


r.post("/", async (req, res) => {
    const unitCount = await req.db.units.count({ user_id: req.userId });
    if (unitCount >= ServerConstants.MaxUnitCount) {
        res.status(400).json({ error: "Max unit count reached" });
        return;
    }

    // stats distribution
    const reqStats = {
        maxHealthPoints: 0,
        attack: 0,
        defence: 0,
        healPower: 0
    };

    const bodyStats = req.body.stats || {};

    let statsSum = 0;
    for (let stat of Object.keys(reqStats)) {
        const v = Math.min(+bodyStats[stat] || 0, 0);
        statsSum += v;        
        reqStats[stat] += 10 + v;
    }

    if (statsSum > 5) {
        res.status(400).json({ error: "Wrong stat distribution" });
        return;
    }

    // create unit
    const unitInsertResult = await req.db.units.insertOne({
        user_id: req.userId,
        creationTime: new Date(),
        name: req.body.name || `Unit ${+new Date()}`,
        level: 1,
        stats: reqStats,
        skills: [
            { name: "d1", level: 1, key: "Q" },
            { name: "d2", level: 1, key: "R" },
            { name: "h1", level: 1, key: "E" },
            { name: "h2", level: 1, key: "F" },
            { name: "s1", level: 10, key: "1" },
            { name: "s2", level: 10, key: "2" },
            { name: "s3", level: 10, key: "3" }
        ]
    });

    res.json({id: unitInsertResult.insertedId});
});


r.get("/:unitId", parseUnitId, async (req, res) => {
    const unit = await req.db.units.findOne({ _id: req.unitId });
    
    res.json({
        id: unit._id,
        name: unit.name
    });
});


r.use("/:unitId/battle-requests", parseUnitId, battleRequestsApi);


export default r;
