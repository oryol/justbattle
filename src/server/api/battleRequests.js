import express from "express";

import BattleFormat from "../../engine/battle-format";


const r = express.Router();


r.get("/", async (req, res) => {
    const battleRequests = await req.db.battleRequests
        .find({ "unit.id": req.unitId })
        .toArray();

    const now = new Date();

    res.json(battleRequests
        .map(r => ({
            id: r._id,
            creationTime: r.creationTime,
            time: now - r.creationTime,
            format: r.format
        }))
        .sort(BattleFormat.compareFormats));
});


r.post("/", async (req, res) => {
    const unitId = req.unitId;
    const format = req.body.format;

    const unit = await req.db.units
        .findOne({ _id: req.unitId }, { fields: { user_id: 1, level: 1 } });

    if (!BattleFormat.formats.byName[format]) {
        res.status(400).json({ error: "Invalid format" });
        return;
    }

    const existingReqCount = await req.db.battleRequests.count({ format, "unit.id": unitId });
    if (existingReqCount > 0) {
        res.status(400).json({ error: "Such request already exists" });
        return;
    }

    await req.db.battleRequests.insertOne({
        creationTime: new Date(),
        format, 
        unit: {
            id: unit._id,
            user_id: unit.user_id,
            level: unit.level
        }
    });

    res.json({});
});


r.delete("/:battleRequestId", async (req, res) => {
    const reqId = req.db.toObjectId(req.params.battleRequestId);
    await req.db.battleRequests.deleteOne({ _id: reqId });
    res.json({});
});


export default r;