import express from "express";


const r = express.Router();


r.get("/current", async (req, res) => {
    const user = await req.db.users.findOne({ _id: req.userId }, { fields: { currentBattle_id: 1, _id: 0 }});
    const battleId = user.currentBattle_id;
    if (!battleId) {
        res.json(null);
        return;
    }

    const battle = await req.db.battles.findOne({ _id: battleId }, { fields: { host: 1, _id: 0 } });
    if (!battle) {
        await req.db.users.updateOne(
            { _id: req.userId },
            { $set: { currentBattle_id: null }});
        console.warn("Battle doesn't exist");
        res.json(null);
        return;
    }

    res.json(`${battle.host}_${battleId}`);
});


export default r;